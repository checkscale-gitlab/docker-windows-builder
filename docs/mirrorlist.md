# MirrorLists
[XML](#XML)\
[YML](#YML)\
[JSON](#JSON)

### Description for all lists
the root element is called `versions` and must only include `version` elements.
all `version` elements must have 2 string attributes `file` and `checksum`
this `version` elements require min. 1 self-closing element called `mirror` with a string attribute `url`.

### XML
#### example:
rules: [XSD File](../public/mirrorlist.xsd)
```xml
<?xml version='1.0' encoding='utf-8'?>
<!--
  Authors:      jak1
  Copyright:    (C) 2021 TheManaWorld Developers
  Generated by: Gitlab deploy (manual changes will get overwritten)
-->
<versions>
    <version file="bar-baz.zip" checksum="47ac26e6014b2dea92ed3611161177dadbce50615f34b1e875d0f9e13b6a6d75">
        <mirror url="https://mirrorurl.foo/bar-baz.zip"/>
        <mirror url="https://anothermirrorurl.foo/bar-baz.zip"/>
    </version>
    <!-- ... -->
</versions>
```
###### (the comment on top of the file is not required but should have always the same format)
### YML
not implemented (nor required) yet

### JSON
#### example:
attributes gets a `-` before there name that prevents "file" to do weird stuff
```json
{
 "#comment": "Authors: jak1 Copyright: (C) 2021 TheManaWorld Developers Generated by: Gitlab deploy (manual changes will get overwritten)",
 "versions": {
  "version": {
   "-file": "bar-baz.zip",
   "-checksum": "47ac26e6014b2dea92ed3611161177dadbce50615f34b1e875d0f9e13b6a6d75",
   "mirror": [
    {
     "-url": "https://mirrorurl.foo/bar-baz.zip"
    },
    {
     "-url": "https://anothermirrorurl.foo/bar-baz.zip"
    }
   ]
  }
 }
}
```
###### (the comment on top of the file is not required but should have always the same format)